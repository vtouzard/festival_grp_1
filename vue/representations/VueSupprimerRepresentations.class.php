<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;

/**
 * Page de suppression d'un établissement donné
 * @author prof
 * @version 2018
 */
class VueSupprimerRepresentations extends VueGenerique {

    /** @var Representation identificateur du groupe à afficher */
    private $uneRepresentation;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        //Permet d'aficher l'entête
        include $this->getEntete();
        ?>
        <br><center>Voulez-vous vraiment supprimer la représentation <?= $this->uneRepresentation->getId() ?> ?
            <h3><br>
                <!-- Si l'on choisi oui, on efface de la bdd la représentation-->
                <a href="index.php?controleur=representations&action=validerSupprimer&id=<?= $this->uneRepresentation->getId() ?>">Oui</a>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <!-- Si l'on choisi non, on retourne à la page liste représentation-->
                <a href="index.php?controleur=representations">Non</a></h3>
        </center>
        <?php
        //Permet d'afficher le pied de page
        include $this->getPied();
    }

    function setUneRepresentation(Groupe $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }

}
