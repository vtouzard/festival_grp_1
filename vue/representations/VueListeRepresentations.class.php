<?php
/**
 * Description Page de consultation de la liste des représentations
 * -> affiche un tableau constitué d'une ligne d'entête et d'une ligne par représentation
 * @author grp1
 * @version 2019
 */
namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;

class VueListeRepresentations extends VueGenerique {
    
    private $lesRepresentations;
    
    private $lesDates;

    public function __construct() {
        parent::__construct();
    }

    //Permet d'afficher le tableau comprenant les représentations
    public function afficher() {
        include $this->getEntete();
        ?>
        <strong>Programme par jours</strong><br/>
            <?php
            $this->laDate = 0;
            /* Pour chaque représentation lu dans la base de données
               on récupère leurs informations */
            foreach ($this->lesRepresentations as $key => $representations) {
                ?>
                <strong><?= $key ?></strong>
                <table width="45%" cellspacing="0" cellpadding="0" class="tabQuadrille">
                    <tr class="enTeteTabQuad">
                        <td width="25%">Lieu</td>
                        <td width="25%">Groupe</td>
                        <td width="25%">Heure Début</td>
                        <td width="25%">Heure Fin</td>
                        <td width="25%"></td>
                        <td width="25%"></td>
                    </tr>
                <?php
                foreach ($representations as $uneRepresentation) {
                    ?>
                    <!-- Permet d'obtenir les informations des représentations -->
                    <tr class="ligneTabQuad">
                        <td><?= $uneRepresentation->getLieu()->getNom() ?></td>
                        <td><?= $uneRepresentation->getGroupe()->getNom()?></td>
                        <td><?= $uneRepresentation->getHeureDebut() ?></td>
                        <td><?= $uneRepresentation->getHeureFin() ?></td>
                        <td><a href="index.php?controleur=representations&action=modifier&id=<?= $uneRepresentation->getId() ?>">
                    Modifier
                </a></td>
                        <td><a href="index.php?controleur=representations&action=supprimer&id=<?= $uneRepresentation->getId() ?>">
                    Supprimer
                </a></td>
                    </tr>
                <?php
                }
                ?>
                </table><br>
                <?php
            }
        ?>
        <a href="index.php?controleur=representations&action=creer">Créer une représentation</a>
        <?php
        include $this->getPied();
    }

    public function setLesRepresentations(array $lesRepresentations) {
        $this->lesRepresentations = $lesRepresentations;
    }
    
    public function setLesDates(array $lesDates){
        $this->lesDates = $lesDates;
    }
}

