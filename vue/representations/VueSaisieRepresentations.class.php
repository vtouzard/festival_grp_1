<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;

/**
 * Description Page de saisie/modification d'un établissement donné
 * @author grp1
 * @version 2018
 */
class VueSaisieRepresentations extends VueGenerique {

    /** @var Representations representation à afficher */
    private $uneRepresentation;

    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;

    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;

    /** @var string à afficher en tête du tableau */
    private $message;

    public function __construct() {
        parent::__construct();
    }

    //Permet d'afficher l'entête
    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=representations&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="85%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">
                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>

                <?php
                // En cas de création, l'id est accessible à la saisie           
                if ($this->actionRecue == "creer") {
                    // On a le souci de ré-afficher l'id tel qu'il a été saisi
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" value="<?= $this->uneRepresentation->getId() ?>" name="id" size ="10" maxlength="8"></td>
                    </tr>
                    <?php
                } else {
                    // sinon l'id est dans un champ caché 
                    ?>
                    <tr>
                        <td><input type="hidden" value="<?= $this->uneRepresentation->getId(); ?>" name="id"></td><td></td>
                    </tr>
                    <?php
                }
                ?>
                    
                    <!-- Permet d'afficher les différents champs à remplir/modifier-->
                <tr class="ligneTabNonQuad">
                    <td> Groupe* : </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getGroupe() ?>" name="nom" size="50" 
                               maxlength="45">
                    </td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Lieu*: </td>
                    <td><input type="text" value="<?= $this->unGroupe->getLieu() ?>" name="lieu" size="50" 
                               maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Date*: </td>
                    <td><input type="text" value="<?= $this->unGroupe->getDate() ?>" name="date" size="50" 
                               maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure début*: </td>
                    <td><input type="text" value="<?= $this->unGroupe->getHeureDebut() ?>" name="heureDebut" 
                               size="50" maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure fin*: </td>
                    <td><input type="text" value="<?= $this->unGroupe->getHeureFin() ?>" name="heureFin" 
                               size="7"></td>
                </tr>
            </table>
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <!-- Permet de retourner à la page précèdente à savoir la liste des representations-->
            <a href="index.php?controleur=representations&action=listerepresentations">Retour</a>
        </form>
        <?php
        //permet d'afficher le pied de page
        include $this->getPied();
    }

    public function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }


    public function setActionRecue(string $action) {
        $this->actionRecue = $action;
    }

    public function setActionAEnvoyer(string $action) {
        $this->actionAEnvoyer = $action;
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }

}
