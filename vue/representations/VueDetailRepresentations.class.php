<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representations;

/**
 * Description Page de consultation d'un établissement donné
 * @author grp1
 * @version 2018
 */
class VueDetailRepresentations extends VueGenerique {

    /** @var representations identificateur de la représentation à afficher */
    private $uneRepresentation;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();

        ?>
        <br>
        <table width='60%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'> 
            <tr class='enTeteTabNonQuad'>
                <td colspan='3'><strong><?= $this->uneRepresentation->getGroupe() ?></strong></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td  width='20%'> Lieu </td>
                <td><?= $this->uneRepresentation->getLieu() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Date : </td>
                <td><?= $this->uneRepresentation->getDate() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Heure début : </td>
                <td><?= $this->uneRepresentation->getHeureDebut() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Heure fin : </td>
                <td><?= $this->uneRepresentation->getHeureFin() ?></td>
           </tr>
        </table>
        <br>
         
        <!-- Permet de retourner à la page précèdente-->
        <a href='index.php?controleur=representations&action=listeRepresentations'>Retour</a>
        <?php
        //Permet d'afficher le pied de la page
        include $this->getPied();
    }

    function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }


}
