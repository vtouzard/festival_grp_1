<?php

namespace vue\groupes;

use vue\VueGenerique;
use modele\metier\Groupe;

/**
 * Description Page de consultation d'un établissement donné
 * @author prof
 * @version 2018
 */
class VueDetailGroupes extends VueGenerique {

    /** @var Etablissement identificateur de l'établissement à afficher */
    private $unGroupe;

    public function __construct() {
        parent::__construct();
    }

    //Permet d'afficher l'entête
    public function afficher() {
        include $this->getEntete();

        ?>

<!--Permet de créer un tableau dans lequel on affiche le nom, l'id, le nb de personnes,
le nom du Pays ainsi que l'hébergement du groupe choisi -->
        <br>
        <table width='60%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'> 
            <tr class='enTeteTabNonQuad'>
                <td colspan='3'><strong><?= $this->unGroupe->getNom() ?></strong></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td  width='20%'> Id: </td>
                <td><?= $this->unGroupe->getId() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Nombre de personnes: </td>
                <td><?= $this->unGroupe->getNbPers() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Nom Pays: </td>
                <td><?= $this->unGroupe->getNomPays() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Hebergement: </td>
                <td><?= $this->unGroupe->getHebergement() ?></td>
            </tr>
        </table>
        <br>
        
        <!-- Permet de retourner à la page précèdente-->
        <a href='index.php?controleur=groupes&action=listeGroupes'>Retour</a>
        <?php
        //Permet d'afficher le pied de la page
        include $this->getPied();
    }

    function setUnGroupe(Groupe $unGroupe) {
        $this->unGroupe = $unGroupe;
    }


}
