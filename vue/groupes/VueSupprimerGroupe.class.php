<?php

namespace vue\groupes;

use vue\VueGenerique;
use modele\metier\Groupe;

/**
 * Page de suppression d'un établissement donné
 * @author prof
 * @version 2018
 */
class VueSupprimerGroupe extends VueGenerique {

    /** @var Groupe identificateur du groupe à afficher */
    private $unGroupe;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        //Permet d'aficher l'entête
        include $this->getEntete();
        ?>
        <br><center>Voulez-vous vraiment supprimer le groupe <?= $this->unGroupe->getNom() ?> ?
            <h3><br>
                <!-- Si l'on choisi oui, on efface de la bdd le groupe-->
                <a href="index.php?controleur=groupes&action=validerSupprimer&id=<?= $this->unGroupe->getId() ?>">Oui</a>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <!-- Si l'on choisi non, on retourne à la page liste groupe-->
                <a href="index.php?controleur=groupes">Non</a></h3>
        </center>
        <?php
        //Permet d'afficher le pied de page
        include $this->getPied();
    }

    function setUnGroupe(Groupe $unGroupe) {
        $this->unGroupe = $unGroupe;
    }

}
