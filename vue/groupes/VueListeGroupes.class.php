<?php
/**
 * Description Page de consultation de la liste des groupes
 * -> affiche un tableau constitué d'une ligne d'entête et d'une ligne par groupes
 * @author Alex
 * @version 2019
 */
namespace vue\groupes;

use vue\VueGenerique;

class VueListeGroupes extends VueGenerique {
    
    /** @var array liste des établissements à afficher avec leur nombre d'atttributions */
    private $lesGroupesAvecNbAttributions;
    

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br>
        <table width="55%" cellspacing="0" cellpadding="0" class="tabNonQuadrille" >
            <tr class="enTeteTabNonQuad">
                <td colspan="7"><strong>Groupes</strong></td>
            </tr>
            <?php
            /* Pour chaque établissement lu dans la base de données
               on récupère leurs informations */
            foreach ($this->lesGroupesAvecNbAttributions as $unGroupe) {
                $unGroupe = $unGroupe["Grp"];
                $id = $unGroupe->getId();
                $nom = $unGroupe->getNom();
                $nbPers = $unGroupe->getNbPers();
                $pays = $unGroupe->getNomPays();
                $hebergement = $unGroupe->getHebergement();
                ?>
            
            <!-- On affiche les informations pour chaque groupe-->
                <tr class="ligneTabNonQuad" >
                    <td width="52%" ><?= $nom ?></td>
                    <td width="52%" ><?= $nbPers ?></td>
                    <td width="52%" ><?= $pays ?></td>
                    <td width="52%" ><?= $hebergement ?></td>

                    <!-- Permet d'afficher dans le tableau trois options : Voir détail, Modifier, Supprimer -->
                    <td width="16%" align="center" > 
                        <a href="index.php?controleur=groupes&action=detailGroupe&id=<?= $id ?>" >
                            Voir détail</a>
                    </td>

                    <td width="16%" align="center" > 
                        <a href="index.php?controleur=groupes&action=modifierGroupe&id=<?= $id ?>" >
                            Modifier
                        </a>
                    </td>
                    <td width="16%" align="center" > 
                        <a href="index.php?controleur=groupes&action=supprimerGroupe&id=<?= $id ?>" >
                            Supprimer
                        </a>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <br>
        
        <!-- Permet à l'utilisateur de pouvoir créer un groupe en cliquant sur le lien -->
        <a href="index.php?controleur=groupes&action=creerGroupe" >
            Création d'un groupes</a >
        <?php
        //Permet d'afficher le pied de la page
        include $this->getPied();
    }

    function setLesGroupes($lesGroupesAvecNbAttributions) {
        $this->lesGroupesAvecNbAttributions = $lesGroupesAvecNbAttributions;
    }

}
