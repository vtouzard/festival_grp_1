<?php

namespace vue\groupes;

use vue\VueGenerique;
use modele\metier\Groupe;

/**
 * Description Page de saisie/modification d'un établissement donné
 * @author prof
 * @version 2018
 */
class VueSaisieGroupes extends VueGenerique {

    /** @var Etablissement établissement à afficher */
    private $unGroupe;

    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;

    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;

    /** @var string à afficher en tête du tableau */
    private $message;

    public function __construct() {
        parent::__construct();
    }

    //Permet d'afficher l'entête
    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=groupes&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="85%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">
                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>

                <?php
                // En cas de création, l'id est accessible à la saisie           
                if ($this->actionRecue == "creer") {
                    // On a le souci de ré-afficher l'id tel qu'il a été saisi
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" value="<?= $this->unGroupe->getId() ?>" name="id" size ="10" maxlength="8"></td>
                    </tr>
                    <?php
                } else {
                    // sinon l'id est dans un champ caché 
                    ?>
                    <tr>
                        <td><input type="hidden" value="<?= $this->unGroupe->getId(); ?>" name="id"></td><td></td>
                    </tr>
                    <?php
                }
                ?>
                    
                    <!-- Permet d'afficher les différents champs à remplir/modifier-->
                <tr class="ligneTabNonQuad">
                    <td> Nom*: </td>
                    <td><input type="text" value="<?= $this->unGroupe->getNom() ?>" name="nom" size="50" 
                               maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Identité Responsable*: </td>
                    <td><input type="text" value="<?= $this->unGroupe->getIdentite() ?>" name="identiteResponsable" size="50" 
                               maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Adresse postale*: </td>
                    <td><input type="text" value="<?= $this->unGroupe->getAdresse() ?>" name="adressePostale" size="50" 
                               maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Nombre de personnes*: </td>
                    <td><input type="text" value="<?= $this->unGroupe->getNbPers() ?>" name="nombrePersonnes" 
                               size="50" maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Pays*: </td>
                    <td><input type="text" value="<?= $this->unGroupe->getNomPays() ?>" name="nomPays" 
                               size="7"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Hebergement*: </td>
                    <td><input type="text" value="<?= $this->unGroupe->getHebergement() ?>" name="hébergement" size="40" 
                               maxlength="35"></td>
                </tr>
            </table>
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <!-- Permet de retourner à la page précèdente à savoir la liste des groupes-->
            <a href="index.php?controleur=groupes&action=listeGroupes">Retour</a>
        </form>
        <?php
        //permet d'afficher le pied de page
        include $this->getPied();
    }

    public function setUnGroupe(Groupe $unGroupe) {
        $this->unGroupe = $unGroupe;
    }


    public function setActionRecue(string $action) {
        $this->actionRecue = $action;
    }

    public function setActionAEnvoyer(string $action) {
        $this->actionAEnvoyer = $action;
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }

}
