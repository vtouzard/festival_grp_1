<?php

namespace modele\dao;

use modele\metier\Lieu;
use \PDO;
use PDOStatement;

class LieuDAO{
    
    /**
     * crée un objet métier à partir d'un enregistrement
     * @param array $enreg
     * @return Utilisateur objet métier obtenu
     */
    protected static function enregVersMetier(array $enreg) {
        $id_lieu = $enreg['ID_LIEU'];
        $lib_lieu = $enreg['LIB_LIEU'];
        $lib_adr = $enreg['LIB_ADR'];
        $Capacite = $enreg['CAPACITE'];
        $objetMetier = new Lieu($id_lieu, $lib_lieu, $lib_adr, $Capacite);
        return $objetMetier;
    }
    
      /**
     * Complète une requête préparée
     * les paramètres de la requête associés aux valeurs des attributs d'un objet métier
     * @param Lieu $objetMetier
     * @param PDOStatement $stmt
     */
    protected static function metierVersEnreg(Lieu $objetMetier, PDOStatement $stmt) {
        $stmt->bindValue(':id_lieu', $objetMetier->getId());
        $stmt->bindValue(':lib_lieu', $objetMetier->getNom());
        $stmt->bindValue(':lib_adr', $objetMetier->getAdresse());
        $stmt->bindValue(':Capacite', $objetMetier->getCapacite());
    }
    
     /**
     * Retourne la liste de tous les lieux
     * @return array tableau d'objets de type Lieu
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Recherche un lieu selon la valeur de son identifiant
     * @param string $id_lieu
     * @return Lieu le lieu trouvé ; null sinon
     */
     public static function getOneById($id_lieu) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE id_lieu = :id_lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id_lieu', $id_lieu);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Lieu $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Lieu $objet) {
        $requete = "INSERT INTO Lieu VALUES (:id_lieu, :lib_lieu, :lib_adr, :Capacite)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    
     /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Lieu $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update($id_lieu, Lieu $objet) {
        $ok = false;
        $requete = "UPDATE Lieu SET LIB_LIEU=:lib_lieu,
           LIB_ADR=:lib_adr, CAPACITE=:Capacite
           WHERE ID_LIEU=:id_lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id_lieu', $id_lieu);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    
     /**
     * Détruire un enregistrement de la table Lieu d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id_lieu) {
        $ok = false;
        $requete = "DELETE FROM Lieu WHERE ID_LIEU = :id_lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id_lieu', $id_lieu);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
    
       /**
     * Permet de vérifier s'il existe ou non un lieu ayant déjà le même identifiant dans la BD
     * @param string $id_lieu identifiant du lieu à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id_lieu) {
        $requete = "SELECT COUNT(*) FROM Etablissement WHERE ID_LIEU=:id_lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id_lieu', $id_lieu);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
    
        /**
     * Permet de vérifier s'il existe ou non un lieu portant déjà le même nom dans la BD
     * En mode modification, l'enregistrement en cours de modification est bien entendu exclu du test
     * @param boolean $estModeCreation =true si le test est fait en mode création, =false en mode modification
     * @param string $id_lieu identifiant du Lieu à tester
     * @param string $lib_lieu nom du Lieu à tester
     * @return boolean =true si le nom existe déjà, =false sinon
     */
    public static function isAnExistingName($estModeCreation, $id_lieu, $lib_lieu) {
        $nom = str_replace("'", "''",  $lib_lieu);
        // S'il s'agit d'une création, on vérifie juste la non existence du nom sinon
        // on vérifie la non existence d'un autre Lieu (id!='$id') portant 
        // le même nom
        if ($estModeCreation) {
            $requete = "SELECT COUNT(*) FROM Lieu WHERE  LIB_LIEU=: lib_lieu";
            $stmt = Bdd::getPdo()->prepare($requete);
            $stmt->bindParam(':lib_lieu', $lib_lieu);
            $stmt->execute();
        } else {
            $requete = "SELECT COUNT(*) FROM Lieu WHERE ID_LIEU=:id_lieu AND ID_LIEU<>:id_lieu";
            $stmt = Bdd::getPdo()->prepare($requete);
            $stmt->bindParam(':id_lieu', $id_lieu);
            $stmt->bindParam(':lib_lieu', $lib_lieu);
            $stmt->execute();
        }
        return $stmt->fetchColumn(0);
    }

}