<?php

namespace modele\dao;

use modele\metier\Representation;
use modele\dao\AttributionDAO;
use modele\dao\LieuDAO;
use modele\dao\GroupeDAO;
use modele\dao\Bdd;
use PDOStatement;
use PDO;

/**
 * Description of EtablissementDAO
 * Classe métier : Etablissement
 * @author prof
 * @version 2017
 */
class RepresentationDAO {

    /**
     * Instancier un objet de la classe Representation à partir d'un enregistrement de la table REPRESENTATION
     * @param array $enreg
     * @return Representation
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID_REPRESENTATION'];
        $groupe = $enreg['ID_GROUPE'];
        $lieu = $enreg['ID_LIEU'];
        $date = $enreg['DATEREPRESENTATION'];
        $heureDebut = $enreg['HEURES_DEBUT'];
        $heureFin = $enreg['HEURES_FIN'];
        $objetGroupe = GroupeDao::getOneById($groupe);
        $objetLieu = LieuDao::getOneById($lieu);
        $uneRep = new Representation($id, $objetGroupe, $objetLieu, $date, $heureDebut, $heureFin);

        return $uneRep;
    }

    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Representation
     * @param type $objetMetier une Representation
     * @param type $stmt requête préparée
     */
    protected static function metierVersEnreg(Representation $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':id_Representation', $objetMetier->getId());
        $stmt->bindValue(':id_groupe', $objetMetier->getGroupe());
        $stmt->bindValue(':id_lieu', $objetMetier->getLieu());
        $stmt->bindValue(':dateRepresentation', $objetMetier->getDate());
        $stmt->bindValue(':heures_debut', $objetMetier->getHeureDebut());
        $stmt->bindValue(':heures_fin', $objetMetier->getHeureFin());
    }

    /**
     * Retourne la liste de toutes les Representations
     * @return array tableau d'objets de type Representation
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Pour chaque enregisterement
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // instancier une Representation et l'ajouter au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    public static function getAllByDate(){
        $requete = "SELECT * FROM Representation ORDER BY dateRepresentation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        $a = [];
        if ($ok) {
            $lastDate = "";
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)){
                $o = self::enregVersMetier($enreg);
                if ($o->getDate() !== $lastDate) $lastDate = $o->getDate();
                $a[$lastDate][] = $o;
            }
        }
        return $a;
    }
    
    public static function getDate(){
        $lesObjets = array();
        $requete = "SELECT dateRepresentation FROM Representation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        $lesObjets[] = $stmt->fetch(PDO::FETCH_ASSOC);
        return $lesObjets;
    }

    /**
     * Recherche une representation selon la valeur de son identifiant
     * @param string $id
     * @return Representation la representation trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE id_Representation = :id";
        $stmt = Bdd::getPdo();
        $stmt = $stmt->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }

    /**
     * Insérer une nouvelle representation dans la table à partir de l'état d'un objet métier
     * @param Representation $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $requete = "INSERT INTO Representation VALUES (:id_Representation, :id_groupe, :id_lieu, :dateRepresentation, :heures_debut, :heures_fin)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Representation $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        $requete = "UPDATE Representation SET ID_GROUPE=:id_groupe, ID_LIEU=:id_lieu,
           DATEREPRESENTATION=:dateRepresentation, HEURES_DEBUT=:heures_debut, HEURES_FIN=heures_fin
           WHERE ID_REPRESENTATION=:id_Representation";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Détruire un enregistrement de la table REPRESENTATION d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation WHERE ID_REPRESENTATION = :id_Representation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id_Representation', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }

    /**
     * Permet de vérifier s'il existe ou non une représentation ayant déjà le même identifiant dans la BD
     * @param string $id identifiant de la représentation à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Representation WHERE ID_REPRESENTATION=:id_Representation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id_Representation', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }

}
