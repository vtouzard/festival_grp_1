<?php
namespace modele\metier;

/**
 * Description of Etablissement
 * un établissement a des capacités d'hébergement à offrir au festival
 * @author prof
 */
class Representation {
    /**
     *
     * @var string
     */
    private $id_representation;
    /**
     * Objet de type Groupe
     * @var Groupe
     */
    private $id_groupe;
    /**
     * Objet de type Lieu
     * @var Lieu
     */
    private $id_lieu;
    /**
     * Date de la représentation
     * @var string 
     */
    private $dateRepresentation;
    /**
     * Heure de début
     * @var string
     */
    private $heures_debut;
    /**
     * Heure de fin
     * @var string
     */
    private $heures_fin;
    
    function __construct($id_representation, $id_groupe, $id_lieu, $dateRepresentation, $heures_debut, $heures_fin) {
        $this->id_representation = $id_representation;
        $this->id_groupe = $id_groupe;
        $this->id_lieu = $id_lieu;
        $this->dateRepresentation = $dateRepresentation; 
        $this->heures_debut = $heures_debut;
        $this->heures_fin = $heures_fin;
    }

    function getId(){
        return $this->id_representation;
    }
    function setId(String $id_representation){
        $this->id_representation = $id_representation;
    } 
    function getGroupe(){
        return $this->id_groupe;
    }
    function setGroupe(Groupe $Groupe){
        $this->Groupe = $id_groupe;
    }
    function getLieu(){
        return $this->id_lieu;
    }
    function setLieu(Lieu $Lieu){
        $this->Lieu = $id_lieu;
    }
    function getDate(){
        return $this->dateRepresentation;
    }
    function setDate(String $dateRepresentation){
        $this->dateRepresentation = $dateRepresentation;
    }
    function getHeureDebut(){
        return $this->heures_debut;
    }
    function setHeureDebut(String $heures_debut){
        $this->heures_debut = $heures_debut;
    }
    function getHeureFin(){
        return $this->heures_fin;
    }
    function setHeureFin(String $heures_fin){
        $this->heures_fin = $heures_fin;
    }
}
