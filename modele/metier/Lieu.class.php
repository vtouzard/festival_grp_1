<?php
namespace modele\metier;

/**
 * Description of Lieu
 * Une instance de Lieu représente le fait qu'une représentation
 * se passe dans un lieu 
 * @author Alex
 */
class Lieu {
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $nom;
    /**
     * @var string 
     */
    private $adresse;
    /**
     * @var int
     */
    private $capacite;
 
    function __construct(string $id, string $nom, string $adresse, int $capacite) {
        $this->id = $id;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->capacite = $capacite;
    }
    function getId(){
        return $this->id;
    }
    function getNom() {
        return $this->nom;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getCapacite() : int {
        return $this->capacite;
    }

    function setId(string $id){
        $this->id = $id;
    }

    function setNom(string $nom) {
        $this->nom = $nom;
    }

    function setAdresse(string $adresse) {
        $this->adresse = $adresse;
    }

    function setCapacite(int $capacite) {
        $this->capacite = $capacite;
    }


}
