<?php

/**
 * Contrôleur de gestion des représentations
 * @author grp1
 * @version 2018
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\RepresentationDAO;
use modele\metier\Representation;
use modele\dao\Bdd;
use vue\representations\VueListeRepresentations;
use vue\representations\VueDetailRepresentations;
use vue\representations\VueSaisieRepresentations;
use vue\representations\VueSupprimerRepresentations;

class CtrlRepresentations extends ControleurGenerique {

    /** controleur= représentations & action= defaut
     * Afficher la liste des représentations      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= représentations & action= liste
     * Afficher la liste des représentations      */
    public function liste() {
        $laVue = new VueListeRepresentations();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des représentations 
        Bdd::connecter();
        $laVue->setLesDates(RepresentationDAO::getDate());
        $laVue->setLesRepresentations(RepresentationDAO::getAllByDate());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representations");
        $this->vue->afficher();
    }

    /** controleur= représentations & action=detail & id=identifiant_représentations
     * Afficher un représentations d'après son identifiant     */
    public function detail() {
        $idRep = $_GET["id"];
        $this->vue = new VueDetailRepresentations();
        // Lire dans la BDD les données de l'établissement à afficher
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idEtab));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }

    /** controleur= représentations & action=creer
     * Afficher le formulaire d'ajout d'un représentations     */
    public function creer() {
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvel representation");
        // En création, on affiche un formulaire vide
        /* @var représentations $uneRep */
        $uneRep = new Representation("", "", "", "", "", "");
        $laVue->setUneRepresentation($uneRep);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }

    /** controleur= représentations & action=validerCreer
     * ajouter d'une représentations dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var représentations $uneRep  : récupération du contenu du formulaire et instanciation d'une représentations */
        $uneRep = new Representation($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['adresseRue'], $_REQUEST['codePostal'], $_REQUEST['ville'], $_REQUEST['tel']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesRep($uneRep, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer la représentation
            RepresentationDAO::insert($uneRep);
            // revenir à la liste des représentations
            header("Location: index.php?controleur=representation&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvel représentation");
            $laVue->setUnEtablissement($uneRep);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - representation");
            $this->vue->afficher();
        }
    }

    /** controleur= représentations & action=modifier $ id=identifiant de la représentation à modifier
     * Afficher le formulaire de modification d'un représentations     */
    public function modifier() {
        $idRep = $_GET["id"];
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        // Lire dans la BDD les données de la représentation à modifier
        Bdd::connecter();
        /* @var Représentations $laRepresentation */
        $laRepresentation = RepresentationDAO::getOneById($idRep);
        $this->vue->setUneRepresentation($laRepresentation);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la représentation : (" . $laRepresentation->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

    /** controleur= représentations & action=validerModifier
     * modifier une représentation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Representations $uneRep  : récupération du contenu du formulaire et instanciation d'une représentation */
        $uneRep = new Representation($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['adresseRue'], $_REQUEST['codePostal'], $_REQUEST['ville'], $_REQUEST['tel']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesEtab($uneRep, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour la représentation
            RepresentationDAO::update($uneRep->getId(), $uneRep);
            // revenir à la liste des représentations
            header("Location: index.php?controleur=representation&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRep);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier la représentation : (" . $unEtab->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - representation");
            $this->vue->afficher();
        }
    }

    /** controleur= représentations & action=supprimer & id=identifiant_représentation
     * Supprimer un représentations d'après son identifiant     */
    public function supprimer() {
        $idRep = $_GET["id"];
        $this->vue = new VueSupprimerRepresentation();
        // Lire dans la BDD les données de la représentation à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRep));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

    /** controleur= représentations & action= validerSupprimer
     * supprimer une représentation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la representation à supprimer");
        } else {
            // suppression de la représentation d'après son identifiant
            EtablissementDAO::delete($_GET["id"]);
        }
        // retour à la liste des représentations
        header("Location: index.php?controleur=representation&action=liste");
    }

    /**
     * Vérification des données du formulaire de saisie
     * @param Représentation $uneRep représentation à vérifier
     * @param bool $creation : =true si formulaire de création d'une nouvelle représentation ; =false sinon
     */
    private function verifierDonneesRep(Representation $uneRep, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $uneRep->getId() == "") || $uneRep->getNom() == "" || $uneRep->getAdresse() == "" || $uneRep->getCdp() == "" ||
                $uneRep->getVille() == "" || $uneRep->getTel() == "" || $uneRep->getNomResp() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRep->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($uneRep->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (EtablissementDAO::isAnExistingId($uneRep->getId())) {
                    GestionErreurs::ajouter("La representation " . $uneRep->getId() . " existe déjà");
                }
            }
        }
    }
}

