-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mar. 15 oct. 2019 à 16:44
-- Version du serveur :  10.3.12-MariaDB
-- Version de PHP :  7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `festival`
--

-- --------------------------------------------------------

--
-- Structure de la table `Representation`
--

CREATE TABLE `Representation` (
  `id_Representation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_groupe` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_lieu` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `Heures_Debut` time NOT NULL,
  `Heures_Fin` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `Representation`
--

INSERT INTO `Representation` (`id_Representation`, `id_groupe`, `id_lieu`, `date`, `Heures_Debut`, `Heures_Fin`) VALUES
('r001', 'g002', 'l001', '2019-10-09', '00:28:00', '00:34:00'),
('r002', 'g001', 'l002', '2019-10-02', '00:24:00', '00:55:00');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Representation`
--
ALTER TABLE `Representation`
  ADD PRIMARY KEY (`id_Representation`),
  ADD KEY `FOREIGN_KEY-Grp` (`id_groupe`),
  ADD KEY `FOREIGN_KEY-Lieu` (`id_lieu`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Representation`
--
ALTER TABLE `Representation`
  ADD CONSTRAINT `FOREIGN_KEY-Grp` FOREIGN KEY (`id_groupe`) REFERENCES `Groupe` (`id`),
  ADD CONSTRAINT `FOREIGN_KEY-Lieu` FOREIGN KEY (`id_lieu`) REFERENCES `Lieu` (`id_lieu`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
