CREATE TABLE representation 
(
id VARCHAR(100) NOT NULL,
groupe VARCHAR(100) NOT NULL,
lieu VARCHAR(100) NOT NULL,
date date NOT NULL,
heureDebut TIME NOT NULL,
heureFin TIME NOT NULL
);

INSERT INTO `representation` (`id`, `groupe`, `lieu`, `date`, `heureDebut`, `heureFin`) VALUES
('r001', 'AC/DC', 'Nantes', '2019-10-11', '20:00:00', '02:00:00'),
('r002', 'Led Zeppelin', 'Saint-Sebastien-Sur-Loire', '2019-10-18', '20:50:00', '00:00:00'),
('r003', 'Les Beatles', 'Paris', '2019-10-25', '12:00:00', '23:00:00'),
('r004', 'Powerwolf', 'Los Angeles', '2019-11-01', '08:00:00', '20:00:00');