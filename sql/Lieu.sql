-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mar. 15 oct. 2019 à 16:43
-- Version du serveur :  10.3.12-MariaDB
-- Version de PHP :  7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `festival`
--

-- --------------------------------------------------------

--
-- Structure de la table `Lieu`
--

CREATE TABLE `Lieu` (
  `id_lieu` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lib_lieu` text COLLATE utf8_unicode_ci NOT NULL,
  `lib_adr` text COLLATE utf8_unicode_ci NOT NULL,
  `Capacité` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `Lieu`
--

INSERT INTO `Lieu` (`id_lieu`, `lib_lieu`, `lib_adr`, `Capacité`) VALUES
('l001', 'Paris', '35 rue des lucioles', 500),
('l002', 'Le petit Port', '9 avenue du port', 305),
('l003', 'Le lieu-dit', '5 rue du Voilier', 900),
('l004', 'La crique hurlante', 'La corbière', 1000);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Lieu`
--
ALTER TABLE `Lieu`
  ADD PRIMARY KEY (`id_lieu`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
